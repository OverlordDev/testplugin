
#include "API/ARK/Ark.h"
#include <string>
#include <vector>
#include <random>
#include <map>
#include <time.h>

class DBItem
{
public:
	DBItem(int id, int group_id, std::string  blueprint, int chance, float durability, int custom_dependency, std::string  Comment)
	{
		this->id = id;
		this->group_id;
		this->blueprint = blueprint;
		this->chance = chance;
		this->durability = durability;
		this->custom_dependency = custom_dependency;
		this->Comment = Comment;
	}
	DBItem()
	{

	}
	~DBItem()
	{

	}
	int id;
	int group_id;
	std::string blueprint;
	int chance;
	double durability;
	int custom_dependency;
	bool isBlueprint;
	std::string  Comment;
};
class Dino
{
public:
	Dino(int id, int group_id, int chance, std::string blueprint, std::string comment)
	{
		this->id = id;
		this->group_id = group_id;
		this->chance = chance;
		this->blueprint = blueprint;
		this->comment = comment;
	}
	Dino()
	{

	}
	~Dino()
	{

	}
	int id;
	int group_id;
	int chance;
	int level;
	std::string blueprint;
	std::string comment;
	//The rest
};
class LootBox
{
	public:
		LootBox(int id, int group_id, std::string name)
		{
			this->id = id;
			this->group_id = group_id;
			this->name = name;
		}
		LootBox()
		{

		}
		~LootBox()
		{

		}
		int id;
		int group_id;
		std::string name;
		int Chance;
		int Points;
};
class LootBoxMenu
{
public:
	LootBoxMenu()
	{

	}
	~LootBoxMenu()
	{

	}
	int lootbox_entry;
	int item_group_id;
	int item_count;
	int dino_group_id;
	int dino_count;
	int points;
	std::string comment;
};
class Player
{
public:
	Player()
	{

	}
	~Player()
	{

	}
	__int64 steamid;
	std::string steamName;
	std::string lootbox_bytes;
	bool isOnline;
	bool canClaimReward;
	int Rank;
	__int64 Points;
	__int64 t_time;
	bool hasClaimedReward;
	std::vector<int> GetPlayerBytes();

	void SetPlayerBytes(std::vector<int> bytes);
	bool HasLootBox(int id);
};
