#include "Utilities.h"


std::vector<int> Player::GetPlayerBytes()
{
	std::string dump;
	std::vector<int> vec;
	for (int i = 0; i < lootbox_bytes.length(); i++)
	{
		if (lootbox_bytes[i] != ' ')
			dump += lootbox_bytes[i];
		else
		{
			vec.push_back(std::stoi(dump));
			dump.clear();
		}
	}
	return vec;
};
void Player::SetPlayerBytes(std::vector<int> bytes)
{
	lootbox_bytes.clear();
	for (int i = 0; i < bytes.size(); i++)
	{
		lootbox_bytes += std::to_string(bytes[i]) + ' ';
	}
}
bool Player::HasLootBox(int id)
{
	std::vector<int> bytes = GetPlayerBytes();
	
	if (bytes[id] != 0)
		return true;

	return false;
}
;
