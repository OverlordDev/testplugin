
#include "API/ARK/Ark.h"
#include "API/UE/Math/ColorList.h"
#include "API/Public/Timer.h"
#include "json.hpp"
#include "DatabaseMgr.h"
#include <algorithm>
#pragma comment(lib, "ArkApi.lib")

#define ONE_DAY 86400
#define SPECIAL_VALUE -1
#define TRUE 1
#define FALSE 0

Database db;
Config conf;
std::vector<LootBox> lootboxes;
std::vector<LootBoxMenu> lootboxmenu;
std::vector<DBItem> items;
std::vector<Dino> dinos;

class local_values
{
public:
	local_values(bool isOnline, bool canClaimReward, int time)
	{
		this->isOnline = isOnline;
		this->canClaimReward = canClaimReward;
		this->t_time = time;
	}
	bool isOnline;
	bool canClaimReward;
	int t_time;
};
std::map<__int64,Player> players;
//Utility
	//Lock
bool lock = false;
void Lock()
{
	lock = true;
}
void Unlock()
{
	lock = false;
}
bool isLocked()
{
	return lock;
}


void AddUpdPlayer(Player &p)
{
	db.PrepareStatement(Stmt::PLAYER_REPL);
	db.SetPlayer(p);
	db.Step();
}
void AddUpdAllPlayers()
{
	for (std::map<__int64, Player>::iterator it = players.begin(); it != players.end(); it++)
	{
		AddUpdPlayer(it->second);
	}
}
void AddUpdItems()
{
	for (std::vector<DBItem>::iterator it = items.begin(); it != items.end(); it++)
	{
		db.PrepareStatement(Stmt::ITEM_REPL);
		db.SetInt(1, it->id);
		db.SetInt(2, it->group_id);
		db.SetString(3, it->blueprint);
		db.SetInt(4, it->chance);
		db.SetDouble(5, it->durability);
		db.SetInt(6, it->custom_dependency);
		db.SetString(7, it->Comment);

		db.Step();
	}
}
void AddUpdDinos()
{

}
void ResetTimer()
{
	db.PrepareStatement(Stmt::PLAYER_UPD_TIME);
	db.SetInt(1, conf.config["PlayedTimeReward"]);
	db.SetBool(2, FALSE);
	db.Step();
}
//Loaders
std::map<__int64, Player> GetAllDBPlayers()
{
	std::map<__int64, Player> playersX;
	db.PrepareStatement(Stmt::PLAYER_SEL_ALL);
	while (db.Step() == SQLITE_OK)
	{
		Player p = db.GetPlayer();
		playersX.insert(std::pair<__int64, Player>(p.steamid, p));
	}
	return playersX;
}
void LoadItems()
{
	items.clear();
	db.PrepareStatement(Stmt::ITEM_SEL_ALL);
	while (db.Step() == SQLITE_ROW)
		items.push_back(db.GetItem());
};
void LoadDinos()
{
	dinos.clear();
	db.PrepareStatement(Stmt::DINO_SEL_ALL);
	while (db.Step() == SQLITE_ROW)
		dinos.push_back(db.GetDino());
};
void LoadLootbox()
{
	lootboxes.clear();
	db.PrepareStatement(Stmt::LOOTBOX_SEL_ALL);
	while (db.Step() == SQLITE_ROW)
	{
		LootBox box = db.GetLootBox();
		lootboxes.push_back(box);
	}
}
void LoadLootboxMenu()
{
	lootboxmenu.clear();
	db.PrepareStatement(Stmt::LOOTBOX_MENU_SEL_ALL);
	while (db.Step() == SQLITE_ROW)
	{
		LootBoxMenu boxmenu = db.GetLootBoxMenu();
		lootboxmenu.push_back(boxmenu);
	}
}
void LoadEntireDatabase()
{
	LoadItems();
	LoadDinos();
	LoadLootbox();
	LoadLootboxMenu();
}
void ReloadPlayerBytes(Player& p)
{
	std::vector<int> playerbytes = p.GetPlayerBytes();
	if (lootboxes.size() == playerbytes.size())
		return;

	if (lootboxes.size() > playerbytes.size())
	{
		for (int i = 0; i < lootboxes.size() - playerbytes.size(); i++)
		{
			playerbytes.push_back(0);
		}
	}
	else
	{
		playerbytes.resize(lootboxes.size());
	}

	p.SetPlayerBytes(playerbytes);
	AddUpdPlayer(p);
}
// Declared Hooks
DECLARE_HOOK(AShooterGameMode_PostLogin, void, AShooterGameMode*, APlayerController *);
void  Hook_AShooterGameMode_PostLogin(AShooterGameMode* _this, APlayerController* NewPlayer)
{
	__int64 guid = ArkApi::GetApiUtils().GetSteamIdFromController(NewPlayer);

	Player p;
	p.steamid = guid;
	std::vector<int> default_byte;

	db.PrepareStatement(Stmt::PLAYER_SEL);
	db.SetInt64(1, p.steamid);
		// Existing player only in db
	if (db.Step() == SQLITE_ROW)
	{
		//Found player in database
		p = db.GetPlayer();
		p.isOnline = TRUE;
		if (p.canClaimReward)
			p.hasClaimedReward = FALSE;
		else
			p.hasClaimedReward = TRUE;
		players.insert(std::pair<__int64, Player>(guid, p));		
	}
	else
	{
		//New player
		db.PrepareStatement(Stmt::LOOTBOX_ROW_COUNT);
		db.Step();
		int row_count = db.GetRowCount();
		default_byte.clear();
		for (int i = 0; i < row_count; i++)
			default_byte.push_back(0);
		p.canClaimReward = FALSE;
		p.isOnline = TRUE;
		p.SetPlayerBytes(default_byte);
		p.Rank = 0;
		p.Points = 0;
		p.t_time = conf.config["PlayedTimeReward"];
		p.steamName = ArkApi::GetApiUtils().GetSteamName(NewPlayer).ToString();
		p.hasClaimedReward = FALSE;
		players.insert(std::pair<__int64, Player>(guid, p));

	}
	AShooterGameMode_PostLogin_original(_this, NewPlayer);
}

DECLARE_HOOK(AShooterGameMode_Logout, void, AShooterGameMode*, AController*);
void Hook_AShooterGameMode_Logout(AShooterGameMode* _this, AController* exiting)
{
	__int64 guid = ArkApi::GetApiUtils().GetSteamIdFromController(exiting);
	if (players.find(guid) != players.end())
	{
		if (players.find(guid)->second.isOnline == FALSE)
			return;

		players[guid].isOnline = FALSE;
		//AddUpdPlayer(players[guid]);
	}
	AShooterGameMode_Logout_original(_this, exiting);
}


void Handle_claimreward(APlayerController* player_controller, FString*, bool)
{
	if (isLocked())
		return;

	__int64 guid = ArkApi::GetApiUtils().GetSteamIdFromController(player_controller);
	AShooterPlayerController* sender = static_cast<AShooterPlayerController*>(player_controller);

	if (players[guid].canClaimReward)
	{
		for (int i = 0; i < lootboxes.size(); i++)
		{
			if (lootboxes[i].group_id == players[guid].Rank)
			{
				db.AddLootBox(players[guid], lootboxes[i], conf.config["BoxAmount"]);
			}
		}
		players[guid].hasClaimedReward = TRUE;
		players[guid].canClaimReward = FALSE;
	}
	else
		ArkApi::GetApiUtils().SendServerMessage(sender, FColorList::Blue, *FString("You have already collected your reward for today!"));
}
void Handle_checkreward(APlayerController* player_controller, FString*, bool)
{
	if (isLocked())
		return;

	__int64 guid = ArkApi::GetApiUtils().GetSteamIdFromController(player_controller);
	AShooterPlayerController* sender = static_cast<AShooterPlayerController*>(player_controller);
	Player p = players[guid];

	if (p.t_time != 0)
	{
		int  hours, minutes;
		minutes = p.t_time / 60;
		hours = minutes / 60;
		std::stringstream buffer;
		buffer << "Time remaining until your next reward: " << int(hours) << " hours " << int(minutes % 60)
			<< " minutes " << int(p.t_time % 60) << " seconds.";
		ArkApi::GetApiUtils().SendServerMessage(sender, FColorList::Blue, buffer.str().c_str());
	}
	else
	{
		if(p.canClaimReward)
			ArkApi::GetApiUtils().SendServerMessage(sender, FColorList::Blue, *FString("You can collect your lootboxes now!"));
		else
			ArkApi::GetApiUtils().SendServerMessage(sender, FColorList::Blue, *FString("You have already collected your reward for today!"));
	}
}

void Handle_gamble(APlayerController* player_controller, FString* message, bool)
{
	if (isLocked())
		return;

	__int64 guid = ArkApi::GetApiUtils().GetSteamIdFromController(player_controller);
	AShooterPlayerController* sender = static_cast<AShooterPlayerController*>(player_controller);
	TArray<FString> parsed;
	message->ParseIntoArray(parsed, L" ", true);
	parsed.IsValidIndex(1);

	if (parsed.IsValidIndex(1))
	{
		if (parsed[1].ToString().find_first_not_of("0123456789") != std::string::npos || parsed[3].ToString().find_first_not_of("0123456789") != std::string::npos)
		{
			ArkApi::GetApiUtils().SendServerMessage(sender, FColorList::Blue, *FString("The format is gamble <lootboxid> "));
			return;
		}
		int lootboxid = std::atoi(parsed[1].ToString().c_str());
		db.GambleBox(lootboxes[lootboxid], players[guid]);
	}
}
void Handle_gogboxs(APlayerController* player_controller, FString*, bool)
{
	if (isLocked())
		return;

	__int64 guid = ArkApi::GetApiUtils().GetSteamIdFromController(player_controller);
	AShooterPlayerController* sender = static_cast<AShooterPlayerController*>(player_controller);

	std::vector<int> byt = players[guid].GetPlayerBytes();
	std::stringstream buffer;

	for (int i = 0; i < byt.size(); i++)
	{
		if (byt[i] != 0)
			buffer << byt[i] << " " << lootboxes[i].name << "; Gamble: " << lootboxes[i].Points << " points" ;
		ArkApi::GetApiUtils().SendServerMessage(sender, FColorList::DustyRose, buffer.str().c_str());
		buffer.clear();
	}
	buffer << "Your total points are: " << players[guid].Points;
	ArkApi::GetApiUtils().SendServerMessage(sender, FColorList::DustyRose, buffer.str().c_str());
	buffer.clear();
}
void Handle_gogopen(APlayerController* player_controller, FString* message, bool)
{
	if (isLocked())
		return;

	//Open a box and reward the player with le items

	TArray<FString> parsed;
	message->ParseIntoArray(parsed, L" ", true);
	AShooterPlayerController* sender = static_cast<AShooterPlayerController*>(player_controller);
	__int64 guid = ArkApi::GetApiUtils().GetSteamIdFromController(player_controller);
	LootBox box;
	if (parsed.IsValidIndex(1))
	{
		std::string boxName = parsed[1].ToString();
		// make everything lowercase -> search in the vector lootboxes of the player and then -1 + open box
		std::transform(boxName.begin(), boxName.end(), boxName.begin(), ::tolower);

		for (std::vector<LootBox>::iterator it = lootboxes.begin(); it != lootboxes.end(); it++)
		{
			std::string boxN = it->name;
			std::transform(boxN.begin(), boxN.end(), boxN.begin(), ::tolower);

			if (boxN == boxName)
			{
				box = *it;
				break;
			}
		}
		if (players[guid].HasLootBox(box.id))
		{
			db.OpenLootBox(players[guid], box, lootboxmenu, items, dinos);
			db.RemoveLootBox(players[guid], box, conf.config["BoxAmount"]);	
		}
		else
		{
			ArkApi::GetApiUtils().SendServerMessage(sender, FColorList::Blue, *FString("You don't have that box in your inventory."));
		}
	}
}

void Handle_gogboxreload(APlayerController* player_controller, FString*, bool)
{
	if (!player_controller->bIsAdmin().Get())
		return;

	Lock();

	__int64 guid = ArkApi::GetApiUtils().GetSteamIdFromController(player_controller);
	lootboxes.clear();
	lootboxmenu.clear();
	LoadLootbox();
	LoadLootboxMenu();
	std::map<__int64, Player> allPlayers = GetAllDBPlayers();
	for (std::map<__int64, Player>::iterator it = allPlayers.begin(); it != allPlayers.end(); it++)
	{
		if (it->second.isOnline)
		{
			//players
			ReloadPlayerBytes(players[it->first]);
		}
		else
		{
			//it
			ReloadPlayerBytes(it->second);
		}
	}
	Unlock();
}
void Handle_givebox(APlayerController* player_controller, FString* message, bool)
{
	if (isLocked())
		return;

	if (!player_controller->bIsAdmin().Get())
		return;
	// Console --> RCON  GiveLootBox <steamid> <boxid> <count>
	TArray<FString> parsed;
	message->ParseIntoArray(parsed, L" ", true);
	AShooterPlayerController* sender = static_cast<AShooterPlayerController*>(player_controller);
	

	if (parsed.IsValidIndex(3)) 
	{
		if (parsed[1].ToString().find_first_not_of("0123456789") != std::string::npos || parsed[3].ToString().find_first_not_of("0123456789") != std::string::npos) 
		{
			ArkApi::GetApiUtils().SendServerMessage(sender, FColorList::Blue, *FString("The format is givelootbox <steamid> <lootboxid> <amount>"));
			return;
		}
		//Values for the reward
		uint64 guid = std::atoi(parsed[1].ToString().c_str());
		int lootboxid = std::atoi(parsed[2].ToString().c_str());
		int amount = std::atoi(parsed[3].ToString().c_str());
		Player p = players[guid];
		db.AddLootBox(p, lootboxes[lootboxid], amount);
		return;
	}
	else
		ArkApi::GetApiUtils().SendServerMessage(sender, FColorList::Blue, *FString("The command takes 3 arguments --> <steamid> <lootboxid> <amount>"));

}
void Handle_setbox(APlayerController* player_controller, FString* message, bool)
{
	if (isLocked())
		return;

	if (!player_controller->bIsAdmin().Get())
		return;

	// Console --> RCON  GiveLootBox <steamid> <boxid> <count>
	TArray<FString> parsed;
	message->ParseIntoArray(parsed, L" ", true);
	AShooterPlayerController* sender = static_cast<AShooterPlayerController*>(player_controller);


	if (parsed.IsValidIndex(3))
	{
		if (parsed[1].ToString().find_first_not_of("0123456789") != std::string::npos || parsed[3].ToString().find_first_not_of("0123456789") != std::string::npos)
		{
			ArkApi::GetApiUtils().SendServerMessage(sender, FColorList::Blue, *FString("The format is givelootbox <steamid> <lootboxid> <amount>"));
			return;
		}
		//Values for the reward
		uint64 guid = std::stoull(parsed[1].ToString(), 0, 10);
		int lootboxid = std::atoi(parsed[2].ToString().c_str());
		int amount = std::atoi(parsed[3].ToString().c_str());
		Player p = players[guid];
		db.AddLootBox(p, lootboxes[lootboxid], amount);
		return;
	}
	else
		ArkApi::GetApiUtils().SendServerMessage(sender, FColorList::Blue, *FString("The command takes 3 arguments --> <steamid> <lootboxid> <amount>"));
}
void Handle_replbox(APlayerController* player_controller, FString* message, bool)
{
	Lock();

	if (!player_controller->bIsAdmin().Get())
		return;
	// Console --> RCON  GiveLootBox <steamid> <boxid> <count>
	TArray<FString> parsed;
	message->ParseIntoArray(parsed, L" ", true);
	AShooterPlayerController* sender = static_cast<AShooterPlayerController*>(player_controller);
	if (parsed.IsValidIndex(5))
	{
		LootBox box;
		box.id = std::atoi(parsed[1].ToString().c_str());
		box.group_id = std::atoi(parsed[2].ToString().c_str());
		box.name = parsed[3].ToString();
		box.Chance = std::atoi(parsed[4].ToString().c_str());
		box.Points = std::atoi(parsed[5].ToString().c_str());
		db.PrepareStatement(Stmt::LOOTBOX_REPL);
		db.DBSetLootBox(box);
		db.Step();
	}

	LoadLootbox(); //load loot boxes again
	Unlock();
}
void Handle_replboxmenu(APlayerController* player_controller, FString* message, bool)
{
	Lock();

	if (!player_controller->bIsAdmin().Get())
		return;
	// Console --> RCON  GiveLootBox <steamid> <boxid> <count>
	TArray<FString> parsed;
	message->ParseIntoArray(parsed, L" ", true);
	AShooterPlayerController* sender = static_cast<AShooterPlayerController*>(player_controller);

	if (parsed.IsValidIndex(7))
	{
		LootBoxMenu boxmenu;
		boxmenu.lootbox_entry = std::atoi(parsed[1].ToString().c_str());
		boxmenu.item_group_id = std::atoi(parsed[2].ToString().c_str());
		boxmenu.item_count = std::atoi(parsed[3].ToString().c_str());
		boxmenu.dino_group_id = std::atoi(parsed[4].ToString().c_str());
		boxmenu.dino_count = std::atoi(parsed[5].ToString().c_str());
		boxmenu.points = std::atoi(parsed[6].ToString().c_str());
		boxmenu.comment = parsed[7].ToString();
		db.Step();
	}

	LoadLootboxMenu();
	Unlock();
}

void MainLoop()
{
	if (isLocked())
		return;

	int time = int(ArkApi::GetApiUtils().GetWorld()->TimeSecondsField());
	
	if (time % ONE_DAY == 0)
	{ 
		//Daily reset
		ResetTimer();
	}
	//Update all local and db players
	auto it = players.begin();
	while(it != players.end())
	{
		//Only for online players
		if (it->second.isOnline == TRUE)
		{
			if (it->second.t_time > 0)
			{
				it->second.t_time -= 1;
			}

			if (it->second.t_time == 0 && it->second.canClaimReward == false && !it->second.hasClaimedReward)
				it->second.canClaimReward = true;

			AddUpdPlayer(it->second);
			it++;
		}
		else if (it->second.isOnline == FALSE)
		{
			AddUpdPlayer(it->second);
			it = players.erase(it++);		
		}
	}
}

void Load()
{
	//Hook
	ArkApi::GetHooks().SetHook("AShooterGameMode.PostLogin", &Hook_AShooterGameMode_PostLogin, &AShooterGameMode_PostLogin_original); //done
	ArkApi::GetHooks().SetHook("AShooterGameMode.Logout", &Hook_AShooterGameMode_Logout,&AShooterGameMode_Logout_original); //done

	//Add chat commands 
	ArkApi::GetCommands().AddChatCommand("/claimreward", &Handle_claimreward); //done
	ArkApi::GetCommands().AddChatCommand("/checkreward", &Handle_checkreward); //done
	ArkApi::GetCommands().AddChatCommand("/gamble", &Handle_gamble); //done
	ArkApi::GetCommands().AddChatCommand("/gogboxs", &Handle_gogboxs); //done
	ArkApi::GetCommands().AddChatCommand("/gogopen", &Handle_gogopen);  //done

	//Admin commands
	ArkApi::GetCommands().AddChatCommand("/replboxmenu", &Handle_replboxmenu);
	ArkApi::GetCommands().AddChatCommand("/replbox", &Handle_replbox);
	ArkApi::GetCommands().AddChatCommand("gogbox.reload", &Handle_gogboxreload);// done
	ArkApi::GetCommands().AddChatCommand("givebox", &Handle_givebox); //done
	ArkApi::GetCommands().AddChatCommand("setbox", &Handle_setbox); //done
	
	API::Timer::Get().RecurringExecute(&MainLoop, 1, -1, false);
	
	//Config
	conf.ReadConfig();

	//DB
	db.LoadDatabase();
	db.LoadStatements();
	//Items, Dinos, Lootbox, LootboxMenu
	LoadEntireDatabase();

}
void Unload()
{
	//Unhook
	ArkApi::GetHooks().DisableHook("AShooterGameMode.PostLogin", &Hook_AShooterGameMode_PostLogin);
	ArkApi::GetHooks().DisableHook("AShooterGameMode.Logout", &Hook_AShooterGameMode_Logout);

	//Remove chat commands
	ArkApi::GetCommands().RemoveChatCommand("/claimreward");
	ArkApi::GetCommands().RemoveChatCommand("/checkreward");

	ArkApi::GetCommands().RemoveChatCommand("/gamble");
	ArkApi::GetCommands().RemoveChatCommand("/gogboxs");
	ArkApi::GetCommands().RemoveChatCommand("/gogopen");

	ArkApi::GetCommands().RemoveChatCommand("/replbox");
	ArkApi::GetCommands().RemoveChatCommand("/replboxmenu");
	ArkApi::GetCommands().RemoveChatCommand("gogbox.reload");
	ArkApi::GetCommands().RemoveChatCommand("givebox");
	ArkApi::GetCommands().RemoveChatCommand("setbox");
}



BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		Load();
		break;
	case DLL_PROCESS_DETACH:
		Unload();
		break;
	}
	return true;
}