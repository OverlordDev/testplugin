
#include "ConfigMgr.h"
#include "sqlite3.h"
#pragma once
#include <time.h>
#include <map>
#include <random>
#include "Utilities.h"
enum Stmt
{
	//Item
	ITEM_INS,
	ITEM_SEL,
	ITEM_DEL,
	ITEM_UPD,
	ITEM_SEL_ALL,
	ITEM_ROW_COUNT,
	ITEM_REPL,

	//Dino
	DINO_INS,
	DINO_SEL,
	DINO_DEL,
	DINO_UPD,
	DINO_SEL_ALL,
	DINO_REPL,

	//LootBox
	LOOTBOX_INS,
	LOOTBOX_SEL,
	LOOTBOX_DEL,
	LOOTBOX_UPD,
	LOOTBOX_SEL_ALL,
	LOOTBOX_REPL,
	LOOTBOX_ROW_COUNT,
	//LootBoxMenu
	LOOTBOX_MENU_INS,
	LOOTBOX_MENU_SEL,
	LOOTBOX_MENU_DEL,
	LOOTBOX_MENU_UPD,
	LOOTBOX_MENU_SEL_ALL,
	LOOTBOX_MENU_REPL,

	PLAYER_INS,
	PLAYER_SEL,
	PLAYER_DEL,
	PLAYER_UPD,
	PLAYER_UPD_TIME,
	PLAYER_SEL_ALL,
	PLAYER_REPL
};

class Database
{
public:
	std::string db_path = ArkApi::Tools::GetCurrentDir() + "/ArkApi/Plugins/TestPlugin/";
	sqlite3_stmt* stmt;
	sqlite3* db;
	std::map<Stmt, std::string> PreparedStatement;
	int rc;
	bool LoadDatabase();
	bool UnloadDatabase();
	void AddStatement(Stmt index, std::string str);
	void LoadStatements();
	void PrepareStatement(Stmt index);
	int Step();

	void SetInt(int index, int value);
	void SetInt64(int index, sqlite3_int64 value);
	void SetDouble(int index, double value);
	void SetText(int index, const char* value);
	void SetString(int index, std::string str);
	void SetBool(int index, bool value);
	//Handlers
	DBItem GetItem();
	Dino GetDino();
	LootBox GetLootBox();
	Player GetPlayer();
	void SetPlayer(Player& p);
	LootBoxMenu GetLootBoxMenu();
	int GetRowCount();
	void OpenLootBox(Player& p, LootBox& box, std::vector<LootBoxMenu> lootboxmenu,
		std::vector<DBItem> items, std::vector<Dino> dinos);
	void AddLootBox(Player& p, LootBox& box, int amount);
	void RemoveLootBox(Player& p, LootBox& box, int amount);
	void SetLootBox(Player& p, LootBox& box, int amount);
	void DBSetLootBox(LootBox& box);
	void SetLootBoxMenu(LootBoxMenu& boxmenu);
	void GambleBox(LootBox& box, Player& p);
};
