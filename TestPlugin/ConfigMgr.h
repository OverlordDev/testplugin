#pragma once
#include "json.hpp"
#include <fstream>
#include "API/ARK/Ark.h"
#include "API/UE/Math/ColorList.h"

class Config
{
public:
	Config()
	{

	}
	~Config()
	{

	}
	nlohmann::json config;
	void ReadConfig();
	void ReloadConfig();
	void reloadConfig(APlayerController* player_controller, FString*, bool);
	
};