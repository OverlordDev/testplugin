
#include "DatabaseMgr.h"
void RandomSeed()
{
	srand(time(NULL));
};
bool Database::LoadDatabase()
{
	Config conf;
	conf.ReadConfig();
	std::string str = conf.config["DatabaseName"];
	db_path += str + ".db";

	rc = sqlite3_open(db_path.c_str(), &db);

	if (rc)
	{
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		return false;
	}
	else
	{
		fprintf(stdout, "Opened database successfully\n");
		return true;
	}
}
bool Database::UnloadDatabase()
{
	if (sqlite3_close(db) == SQLITE_OK)
		return true;
	else
		return false;
	
}
void Database::AddStatement(Stmt index, std::string str)
{
	PreparedStatement.insert(std::pair<Stmt, std::string>(index, str));
}
void Database::LoadStatements()
{
	AddStatement(Stmt::ITEM_SEL, "SELECT * FROM Item_template WHERE id = ?");
	AddStatement(Stmt::ITEM_DEL, "DELETE FROM Item_template WHERE id = ?");
	AddStatement(Stmt::ITEM_INS, "INSERT INTO Item_template (id, groupd_id, blueprint, chance, durability, custom_dependency, isBlueprint, Comment) VALUES(?, ?, ?, ?, ?, ?, ?)");
	AddStatement(Stmt::ITEM_UPD, "UPDATE Item_template SET group_id = ?, blueprint = ?, chance = ?, durability = ?, custom_dependency = ?, isBlueprint = ?, Comment = ? WHERE id = ?");
	AddStatement(Stmt::ITEM_SEL_ALL, "SELECT * FROM Item_template");
	AddStatement(Stmt::ITEM_ROW_COUNT, "SELECT count(*) FROM Item_template");
	AddStatement(Stmt::ITEM_REPL, "REPLACE INTO Item_template (id, group_id, blueprint, chance, durability, custom_dependency, isBueprint, Comment ) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");

	AddStatement(Stmt::DINO_SEL, "SELECT * FROM Dino_template WHERE id = ?");
	AddStatement(Stmt::DINO_DEL, "DELETE FROM Dino_template WHERE id = ?");
	AddStatement(Stmt::DINO_INS, "INSERT INTO  Dino_template (id, group_id, chance, level, blueprint, Comment) VALUES(?, ?, ?, ?, ?, ?)");
	AddStatement(Stmt::DINO_UPD, "UPDATE Dino_template SET group_id = ?, chance = ?, level = ?, blueprint = ?, Comment = ? WHERE id = ?");
	AddStatement(Stmt::DINO_SEL_ALL, "SELECT * FROM Dino_template ");
	AddStatement(Stmt::DINO_REPL, "REPLACE INTO Dino_template (id, group_id, chance, level, blueprint, Comment) VALUES(?, ?, ?, ?, ?)");

	AddStatement(Stmt::LOOTBOX_SEL, "SELECT * FROM Lootbox_template WHERE id = ?");
	AddStatement(Stmt::LOOTBOX_DEL, "DELETE FROM Lootbox_template WHERE id = ?");
	AddStatement(Stmt::LOOTBOX_INS, "INSERT INTO Lootbox_template (id, group_id, name, Chance, Points) VALUES(?, ?, ?, ?, ?)");
	AddStatement(Stmt::LOOTBOX_UPD, "UPDATE Lootbox_template SET group_id =?, name = ?, Chance = ?, Points = ? WHERE id = ?");
	AddStatement(Stmt::LOOTBOX_SEL_ALL, "SELECT * FROM Lootbox_template");
	AddStatement(Stmt::LOOTBOX_REPL, "REPLACE INTO Lootbox_template (id, group_id, name, Chance, Points) VALUES(?, ?, ?, ?, ?)");
	AddStatement(Stmt::LOOTBOX_ROW_COUNT, "SELECT count(*) FROM Lootbox_template");

	AddStatement(Stmt::LOOTBOX_MENU_SEL, "SELECT * FROM lootbox_menu WHERE lootbox_entry = ?");
	AddStatement(Stmt::LOOTBOX_MENU_DEL, "DELETE FROM lootbox_menu WHERE lootbox_entry = ?");
	AddStatement(Stmt::LOOTBOX_MENU_INS, "INSERT INTO lootbox_menu (lootbox_entry, item_group_id, item_count, dino_group_id, dino_count, points, Comment) VALUES(?, ?, ?, ?, ?, ?, ?)");
	AddStatement(Stmt::LOOTBOX_MENU_UPD, "UPDATE lootbox_menu SET lootbox_entry = ?, item_group_id = ?, item_count = ?, dino_group_id = ?, dino_count = ?, points = ?, Comment = ? WHERE lootbox_entry = ?");
	AddStatement(Stmt::LOOTBOX_MENU_SEL_ALL, "SELECT * FROM lootbox_menu");
	AddStatement(Stmt::LOOTBOX_MENU_REPL, "REPLACE INTO lootbox_menu (lootbox_entry, item_group_id, item_count, dino_group_id, dino_count, points, comment) VALUES(?, ?, ?, ?, ?, ?, ?)");

	AddStatement(Stmt::PLAYER_SEL, "SELECT * FROM Player_template WHERE steamid = ?");
	AddStatement(Stmt::PLAYER_DEL, "DELETE FROM Player_template WHERE steamid = ?");
	AddStatement(Stmt::PLAYER_INS, "INSERT INTO Player_template (steamname, lootbox_bytes, isOnline, canClaimReward, Rank, Points, time )  VALUES(?, ?, ?, ?, ?, ?)");
	AddStatement(Stmt::PLAYER_UPD, "UPDATE Player_template SET steamname = ?, lootbox_bytes = ?, isOnline = ?, canClaimReward = ?, Rank, Points, time = ?  WHERE steamid = ?");
	AddStatement(Stmt::PLAYER_UPD_TIME, "UPDATE Player_template SET time = ?  WHERE isOnline = ?");
	AddStatement(Stmt::PLAYER_SEL_ALL, "SELECT * FROM Player_template");
	AddStatement(Stmt::PLAYER_REPL, "REPLACE INTO Player_template (steamid, steamname, lootbox_bytes, isOnline, canClaimReward, Rank, Points, time) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
}
void Database::PrepareStatement(Stmt index)
{
	sqlite3_prepare(db, PreparedStatement.at(index).c_str(), -1, &stmt, NULL);
}
int Database::Step()
{
	return sqlite3_step(stmt);
}
void Database::SetInt64(int index, sqlite3_int64 value)
{
	sqlite3_bind_int64(stmt, index, value);
}
void Database::SetInt(int index, int value)
{
	sqlite3_bind_int(stmt, index, value);
}
void Database::SetDouble(int index, double value)
{
	sqlite3_bind_double(stmt, index, value);
}
void Database::SetText(int index, const char* value)
{
	sqlite3_bind_text(stmt, index, value, -1, free);
}
void Database::SetString(int index, std::string str)
{
	sqlite3_bind_text(stmt, index, str.c_str(), -1, free);
}
void Database::SetBool(int index, bool value)
{
	sqlite3_bind_int(stmt, index, value);
}
DBItem Database::GetItem()
{
	DBItem item;
	item.id = sqlite3_column_int(stmt, 0);
	item.group_id = sqlite3_column_int(stmt, 1);
	item.blueprint = (reinterpret_cast<const char *>(sqlite3_column_text(stmt, 2)));
	item.chance = sqlite3_column_int(stmt, 3);
	item.durability = sqlite3_column_double(stmt, 4);
	item.custom_dependency = sqlite3_column_int(stmt, 5);
	item.isBlueprint = bool(sqlite3_column_int(stmt, 6));
	item.Comment = (reinterpret_cast<const char *>(sqlite3_column_text(stmt, 7)));
	return item;

}
Dino Database::GetDino()
{
	Dino dino;
	dino.id = sqlite3_column_int(stmt, 0);
	dino.group_id = sqlite3_column_int(stmt, 1);
	dino.chance = sqlite3_column_int(stmt, 2);
	dino.level = sqlite3_column_int(stmt, 3);
	dino.blueprint = (reinterpret_cast<const char *>(sqlite3_column_text(stmt, 4)));
	dino.comment = (reinterpret_cast<const char *>(sqlite3_column_text(stmt, 5)));
	return dino;
}
LootBox Database::GetLootBox()
{
	LootBox box;
	box.id = sqlite3_column_int(stmt, 0);
	box.group_id = sqlite3_column_int(stmt, 1);
	box.name = (reinterpret_cast<const char *>(sqlite3_column_text(stmt, 2)));
	box.Chance = sqlite3_column_int(stmt, 3);
	box.Points = sqlite3_column_int(stmt, 4);
	return box;
}
Player Database::GetPlayer()
{
	Player p;
	p.steamid = sqlite3_column_int64(stmt, 0);
	p.steamName = (reinterpret_cast<const char *>(sqlite3_column_text(stmt, 1)));
	p.lootbox_bytes = (reinterpret_cast<const char *>(sqlite3_column_text(stmt, 2)));
	p.isOnline = bool(sqlite3_column_int(stmt, 3));
	p.canClaimReward = bool(sqlite3_column_int(stmt, 4));
	p.Rank = sqlite3_column_int(stmt, 5);
	p.Points = sqlite3_column_int64(stmt, 6);
	p.t_time = sqlite3_column_int64(stmt, 7);
	return p;
}
void Database::SetPlayer(Player& p)
{
	sqlite3_bind_int64(stmt, 1, p.steamid);
	sqlite3_bind_text(stmt, 2, p.steamName.c_str(), -1, free);
	sqlite3_bind_text(stmt, 3, p.lootbox_bytes.c_str(), -1, free);
	sqlite3_bind_int(stmt, 4, p.isOnline);
	sqlite3_bind_int(stmt, 5, p.canClaimReward);
	sqlite3_bind_int(stmt, 6, p.Rank);
	sqlite3_bind_int64(stmt, 7, p.Points);
	sqlite3_bind_int64(stmt, 8, p.t_time);
}
LootBoxMenu Database::GetLootBoxMenu()
{
	LootBoxMenu loot;
	loot.lootbox_entry = sqlite3_column_int(stmt, 0);
	loot.item_group_id = sqlite3_column_int(stmt, 1);
	loot.item_count = sqlite3_column_int(stmt, 2);
	loot.dino_group_id = sqlite3_column_int(stmt, 3);
	loot.dino_count = sqlite3_column_int(stmt, 4);
	loot.points = sqlite3_column_int(stmt, 5);
	loot.comment = (reinterpret_cast<const char *>(sqlite3_column_text(stmt, 6)));
	return loot;
}
int Database::GetRowCount()
{
	return sqlite3_column_int(stmt, 0);
}
void Database::OpenLootBox(Player& p, LootBox& box, std::vector<LootBoxMenu> lootboxmenu,
	std::vector<DBItem> items, std::vector<Dino> dinos)
{
	std::map<int, int> item;
	std::map<int, int> dino;
	for (std::vector<LootBoxMenu>::iterator it = lootboxmenu.begin(); it != lootboxmenu.end(); it++)
	{
		if (it->lootbox_entry == box.id)
		{
			item.insert(std::pair<int, int>(it->item_group_id, it->item_count));
			dino.insert(std::pair<int, int>(it->dino_group_id, it->dino_count));
		}
	}
	// Open box and reward player with dinos and item map
	AShooterPlayerController* sender = ArkApi::GetApiUtils().FindPlayerFromSteamId(p.steamid);
	UShooterCheatManager* cheatManager = static_cast<UShooterCheatManager*>(sender->CheatManagerField());
	//Item reward
	for (std::map<int, int>::iterator Itemit = item.begin(); Itemit != item.end(); Itemit++)
	{
		DBItem NewItem;
		int AmountItem;
		if (cheatManager)
		{
			PrepareStatement(Stmt::ITEM_SEL);
			SetInt(1, Itemit->first);
			if (Step() == SQLITE_ROW)
			{
				NewItem = GetItem();
				AmountItem = 0;
				for (int i = 0; i < Itemit->second; i++)
				{
					RandomSeed();
					int roll = rand() % 100;
					if (NewItem.chance - roll >= 0)
						AmountItem += 1;
				}
			}
		}
		cheatManager->GiveItemToPlayer((int)sender->LinkedPlayerIDField(), &FString(NewItem.blueprint), AmountItem, (float)NewItem.durability, NewItem.isBlueprint);
		std::stringstream buffer;
		buffer << "+" << AmountItem << "x" << " " << NewItem.Comment;
		ArkApi::GetApiUtils().SendServerMessage(sender, FColorList::Green, buffer.str().c_str());
		buffer.clear();
	}
	//Dino Reward
	for (std::map<int, int>::iterator Dinoit = dino.begin(); Dinoit != dino.end(); Dinoit++)
	{
		Dino NewDino;
		int AmountDino;
		if (cheatManager)
		{
			PrepareStatement(Stmt::DINO_SEL);
			SetInt(1, Dinoit->first);
			if (Step() == SQLITE_ROW)
			{
				NewDino = GetDino();
				AmountDino = 0;
				for (int i = 0; i < Dinoit->second; i++)
				{
					RandomSeed();
					int roll = rand() % 100;
					if (NewDino.chance - roll >= 0)
						AmountDino += 1;
				}
			}
		}
		for (int i = 0; i < AmountDino; i++)
			ArkApi::GetApiUtils().SpawnDino(sender, FString(NewDino.blueprint), nullptr, NewDino.level, true, false);
	}
};
void Database::SetLootBox(Player& p, LootBox& box, int amount)
{
	AShooterPlayerController* sender = ArkApi::GetApiUtils().FindPlayerFromSteamId(p.steamid);
	std::vector<int> playerBytes;

	if (box.group_id == p.Rank)
	{
		playerBytes = p.GetPlayerBytes();
		playerBytes[box.id] = amount;
		p.SetPlayerBytes(playerBytes);
	}
	std::stringstream buffer;
	buffer << "LootBox " << box.name << " number set to " << amount << ".";
	ArkApi::GetApiUtils().SendServerMessage(sender, FColorList::Green, buffer.str().c_str());
	buffer.clear();
}
void Database::DBSetLootBox(LootBox& box)
{
	sqlite3_bind_int(stmt, 1, box.id);
	sqlite3_bind_int(stmt, 2, box.group_id);
	sqlite3_bind_text(stmt, 3, box.name.c_str(), -1, free);
	sqlite3_bind_int(stmt, 4, box.Chance);
	sqlite3_bind_int(stmt, 5, box.Points);
}
void Database::SetLootBoxMenu(LootBoxMenu & boxmenu)
{
	sqlite3_bind_int(stmt, 1, boxmenu.lootbox_entry);
	sqlite3_bind_int(stmt, 2, boxmenu.item_group_id);
	sqlite3_bind_int(stmt, 3, boxmenu.item_count);
	sqlite3_bind_int(stmt, 4, boxmenu.dino_group_id);
	sqlite3_bind_int(stmt, 5, boxmenu.dino_count);
	sqlite3_bind_int(stmt, 6, boxmenu.points);
	sqlite3_bind_text(stmt, 7, boxmenu.comment.c_str(), -1, free);
}
void Database::GambleBox(LootBox & box, Player & p)
{
	AShooterPlayerController* sender = ArkApi::GetApiUtils().FindPlayerFromSteamId(p.steamid);
	RandomSeed();
	int roll = rand() % 100;
	if (box.Chance - roll >= 0)
	{
		AddLootBox(p, box, 1);
	}
	else
	{
		std::stringstream buffer;
		buffer << "Your roll was " << roll << " and you need atleast " << box.Chance << " or higher.";
		ArkApi::GetApiUtils().SendServerMessage(sender, FColorList::Green, buffer.str().c_str());
		buffer.clear();
	}
}
void Database::AddLootBox(Player& p, LootBox& box, int amount)
{
	AShooterPlayerController* sender = ArkApi::GetApiUtils().FindPlayerFromSteamId(p.steamid);
	std::vector<int> playerBytes;
	if (box.group_id == p.Rank)
	{
		playerBytes = p.GetPlayerBytes();
		playerBytes[box.id] += amount;
		p.SetPlayerBytes(playerBytes);

		std::stringstream buffer;
		buffer << "+" << amount << "x " << box.name;
		ArkApi::GetApiUtils().SendServerMessage(sender, FColorList::Green, buffer.str().c_str());
		buffer.clear();
	}
};
void Database::RemoveLootBox(Player& p, LootBox& box, int amount)
{
	AShooterPlayerController* sender = ArkApi::GetApiUtils().FindPlayerFromSteamId(p.steamid);
	std::vector<int> playerBytes;
	if (box.group_id == p.Rank)
	{
		playerBytes = p.GetPlayerBytes();
		if (playerBytes[box.id] - amount >= 0)
			playerBytes[box.id] -= amount;
		else
			playerBytes[box.id] = 0;

		p.SetPlayerBytes(playerBytes);

		std::stringstream buffer;
		buffer << "-" << amount << "x " << box.name;
		ArkApi::GetApiUtils().SendServerMessage(sender, FColorList::Red, buffer.str().c_str());
		buffer.clear();
	}	
};